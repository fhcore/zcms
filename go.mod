module gitee.com/chunanyong/zcms

go 1.16

require (
	gitee.com/chunanyong/gouuid v1.3.2
	github.com/blevesearch/bleve/v2 v2.0.3
	github.com/gin-gonic/gin v1.7.1
	github.com/go-ego/gse v0.67.0
)
